{extend name="public/container"}
{block name="content"}

<div class="layui-fluid">
    <div class="layui-row layui-col-space15"  id="app">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <form class="layui-form layui-form-pane" action="">
                        <div class="layui-form-item">

                            <div class="layui-inline">
                                <label class="layui-form-label">关键词</label>
                                <div class="layui-input-block">
                                    <input type="text" name="uid" class="layui-input" placeholder="请输入用户ID">
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-input-inline">
                                    <button class="layui-btn layui-btn-sm layui-btn-normal" lay-submit="search" lay-filter="search">
                                        <i class="layui-icon layui-icon-search"></i>搜索</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--列表-->
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">列表</div>
                <div class="layui-card-body">
                    <div class="layui-btn-container">
                        <a class="layui-btn layui-btn-sm" href="{:Url('index')}">首页</a>
                    </div>
                    <table class="layui-hide" id="List" lay-filter="List"></table>
                    <script type="text/html" id="thumb">
                        {{# if(d.thumb){ }}
                        <img style="cursor: pointer" lay-event='open_image' src="{{d.thumb}}">
                        {{# }else{ }}
                        暂无图片
                        {{# } }}
                    </script>
                    <script type="text/html" id="act">
<!--                        <button class="layui-btn btn-danger layui-btn-xs" lay-event='delstor'>-->
<!--                            <i class="fa fa-times"></i> 删除-->
<!--                        </button>-->
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{__ADMIN_PATH}js/layuiList.js"></script>
{/block}
{block name="script"}
<script>
    setTimeout(function () {
        $('.alert-info').hide();
    },3000);
    //实例化form
    layList.form.render();
    //加载列表
    layList.tableList('List',"{:Url('getlist')}",function (){
        return [
            {field: 'id', title: 'ID', sort: true,event:'id',width:'4%',align:'center'},
            {field: 'uid', title: 'UID',align:'center'},
            {field: 'nickname', title: '昵称',align:'center'},
            {field: 'classname', title: '分类',align:'center'},
            {field: 'title', title: '标题',align:'center'},
            {field: 'thumb', title: '封面',templet:'#thumb',align:'center'},
            {field: 'likes', title: '点赞数',align:'center'},
            {field: 'nums', title: '累计人数',align:'center'},
            {field: 'goods', title: '销售商品数',align:'center'},
            {field: 'start_time', title: '开播时间',align:'center'},
            {field: 'end_time', title: '结束时间',align:'center'},
            {field: 'right', title: '操作',toolbar:'#act',width:'10%',align:'center'},
        ];
    });

    layList.search('search',function(where){
        layList.reload(where,true);
    });
    //点击事件绑定
    layList.tool(function (event,data,obj) {
        switch (event) {
            case 'delstor':
                var url=layList.U({c:'live.liverecord',a:'delete',q:{id:data.uid}});
                $eb.$swal('delete',function(){
                    $eb.axios.get(url).then(function(res){
                        if(res.status == 200 && res.data.code == 200) {
                            $eb.$swal('success',res.data.msg);
                            obj.del();
                        }else
                            return Promise.reject(res.data.msg || '删除失败')
                    }).catch(function(err){
                        $eb.$swal('error',err);
                    });
                })
                break;
            case 'open_image':
                $eb.openImage(data.thumb);
                break;
        }
    })
</script>
{/block}
