<?php

namespace app\admin\controller\live;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\live\Gift as GiftModel;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

/**
 * 产品分类控制器
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Gift extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取分类列表
     *  @return json
     */
    public function gift_list()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['order', '']
        ]);
        return Json::successlayui(GiftModel::getList($where));
    }

    /**
     * 设置产品分类上架|下架
     * @param string $is_show
     * @param string $id
     */
    public function set_show($is_show = '', $id = '')
    {
        ($is_show == '' || $id == '') && Json::fail('缺少参数');
        if (GiftModel::setShow($id, (int)$is_show)) {
            return Json::successful($is_show == 1 ? '显示成功' : '隐藏成功');
        } else {
            return Json::fail(GiftModel::getErrorInfo($is_show == 1 ? '显示失败' : '隐藏失败'));
        }
    }

    /**
     * 快速编辑
     * @param string $field
     * @param string $id
     * @param string $value
     */
    public function set_gift($field = '', $id = '', $value = '')
    {
        $field == '' || $id == '' || $value == '' && Json::fail('缺少参数');
        if (GiftModel::where('id', $id)->update([$field => $value]))
            return Json::successful('保存成功');
        else
            return Json::fail('保存失败');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $field = [
            Form::radio('type', '类型', 0)->options([['label' => '普通', 'value' => 0], ['label' => '豪华', 'value' => 1]]),
            Form::input('name', '名称'),
            Form::input('coin', '价格'),
            Form::frameImageOne('icon', '图标(180*180)', Url::buildUrl('admin/widget.images/index', array('fodder' => 'icon')))->icon('image')->width('100%')->height('500px'),
            Form::radio('swftype', '动图类型', 0)->options([['label' => 'gif', 'value' => 0],['label' => 'svga', 'value' => 1]]),
            Form::frameImageOne('swf', '动图(400*400)', Url::buildUrl('admin/widget.images/index', array('fodder' => 'swf')))->icon('image')->width('100%')->height('500px'),
            Form::input('swftime', '时间'),
            Form::number('sort', '排序'),
            Form::radio('is_show', '状态', 1)->options([['label' => '显示', 'value' => 1], ['label' => '隐藏', 'value' => 0]])
        ];
        $form = Form::make_post_form('添加礼物', $field, Url::buildUrl('save'), 2);
        $this->assign(compact('form'));
        return $this->fetch('public/form-builder');
    }

    /**
     * 保存新建的资源
     *
     * @param \think\Request $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = Util::postMore([
            'type',
            'name',
            'coin',
            ['icon', ''],
            ['swf', ''],
            'sort',
            ['swftype', 0],
            ['swftime', 0],
            ['is_show', 0]
        ], $request);
        if (!$data['name']) return Json::fail('请输入名称');
        if ($data['coin']<=0) return Json::fail('请输入正确价格');
        if ($data['icon'] =='') return Json::fail('请上传图标');
//        if (count($data['icon']) < 1) return Json::fail('请上传图标');
        if ($data['sort'] < 0) $data['sort'] = 0;
  //      $data['icon'] = $data['icon'][0];
        if($data['swftype']==1){
            if ($data['swf'] =='') return Json::fail('请上传动画');
            if ($data['swftime']<=0) return Json::fail('请输入正确时长');
        }
        $data['add_time'] = time();
        GiftModel::create($data);
        return Json::successful('添加成功!');
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $c = GiftModel::get($id);
        if (!$c) return Json::fail('数据不存在!');
        $field = [
            Form::radio('type', '类型', $c->getData('type'))->options([['label' => '普通', 'value' => 0],['label' => '豪华', 'value' => 1]]),
            Form::input('name', '名称', $c->getData('name')),
            Form::input('coin', '价格', $c->getData('coin')),
            Form::frameImageOne('icon', '图标(180*180)', Url::buildUrl('admin/widget.images/index', array('fodder' => 'icon')),$c->getData('icon'))->icon('image')->width('100%')->height('500px'),
            Form::radio('swftype', '动图类型', $c->getData('swftype'))->options([ ['label' => 'gif', 'value' => 0] , ['label' => 'svga', 'value' => 1] ]),
            Form::frameImageOne('swf', '动图(400*400)', Url::buildUrl('admin/widget.images/index', array('fodder' => 'swf')),$c->getData('swf'))->icon('image')->width('100%')->height('500px'),
            Form::input('swftime', '时间',$c->getData('swftime')),
            Form::number('sort', '排序',$c->getData('sort')),
            Form::radio('is_show', '状态', $c->getData('is_show'))->options([['label' => '显示', 'value' => 1], ['label' => '隐藏', 'value' => 0]])
        ];
        $form = Form::make_post_form('编辑礼物', $field, Url::buildUrl('update', array('id' => $id)), 2);

        $this->assign(compact('form'));
        return $this->fetch('public/form-builder');
    }

    /**
     * 保存更新的资源
     *
     * @param \think\Request $request
     * @param int $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $data = Util::postMore([
            'type',
            'name',
            'coin',
            ['icon', ''],
            ['swf', ''],
            'sort',
            ['swftype', 0],
            ['swftime', 0],
            ['is_show', 0]
        ], $request);
        if (!$data['name']) return Json::fail('请输入名称');
        if ($data['coin']<=0) return Json::fail('请输入正确价格');
        if ($data['icon'] =='') return Json::fail('请上传图标');
        if ($data['sort'] < 0) $data['sort'] = 0;
        if($data['swftype']==1){
            if ($data['swf'] =='' ) return Json::fail('请上传动画');
            if ($data['swftime']<=0) return Json::fail('请输入正确时长');
        }
        GiftModel::edit($data, $id);
        return Json::successful('修改成功!');
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if (!GiftModel::delGift($id))
            return Json::fail(GiftModel::getErrorInfo('删除失败,请稍候再试!'));
        else
            return Json::successful('删除成功!');
    }
}
