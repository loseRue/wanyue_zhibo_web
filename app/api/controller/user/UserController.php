<?php

namespace app\api\controller\user;

use app\http\validates\user\AddressValidate;
use app\models\system\SystemCity;
use think\exception\ValidateException;
use app\Request;
use app\models\user\UserSign;
use app\models\store\StoreOrder;
use app\models\store\StoreProductRelation;
use app\models\user\User;
use app\models\user\UserAddress;
use app\models\user\UserBill;
use app\models\user\UserExtract;
use app\models\user\UserNotice;
use wanyue\services\GroupDataService;
use wanyue\services\UtilService;

/**
 * 用户类
 * Class UserController
 * @package app\api\controller\store
 */
class UserController
{

    /**
     * 获取用户信息
     * @param Request $request
     * @return mixed
     */
    public function userInfo(Request $request)
    {
        $info = User::getUserInfo( $request->uid() );
        $broken_time = intval(sys_config('extract_time'));
        $search_time = time() - 86400 * $broken_time;
        //返佣 +
        $brokerage_commission = UserBill::where(['uid' => $info['uid'], 'category' => 'now_money', 'type' => 'brokerage'])
            ->where('add_time', '>', $search_time)
            ->where('pm', 1)
            ->sum('number');
        //退款退的佣金 -
        $refund_commission = UserBill::where(['uid' => $info['uid'], 'category' => 'now_money', 'type' => 'brokerage'])
            ->where('add_time', '>', $search_time)
            ->where('pm', 0)
            ->sum('number');
        $info['broken_commission'] = bcsub($brokerage_commission, $refund_commission, 2);
        if ($info['broken_commission'] < 0)
            $info['broken_commission'] = 0;
        $info['commissionCount'] = bcsub($info['brokerage_price'], $info['broken_commission'], 2);
        if ($info['commissionCount'] < 0)
            $info['commissionCount'] = 0;
        return app('json')->success($info);
    }

    /**
     * 用户资金统计
     * @param Request $request
     * @return mixed
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function balance(Request $request)
    {
        $uid = $request->uid();
        $user['now_money'] = User::getUserInfo($uid, 'now_money')['now_money'];//当前总资金
        $user['recharge'] = UserBill::getRecharge($uid);//累计充值
        $user['orderStatusSum'] = StoreOrder::getOrderStatusSum($uid);//累计消费
        return app('json')->successful($user);
    }

    /**
     * 个人中心
     * @param Request $request
     * @return mixed
     */
    public function user(Request $request)
    {
        $user = User::getUserInfo( $request->uid() );
        $user2=$user;
        $user['couponCount'] = 0;
        $user['like'] = StoreProductRelation::getUserIdCollect($user['uid']);
        $user['orderStatusNum'] = StoreOrder::getOrderData($user['uid']);
        $user['notice'] = UserNotice::getNotice($user['uid']);
//        $user['brokerage'] = UserBill::getBrokerage($user['uid']);//获取总佣金
        $user['recharge'] = UserBill::getRecharge($user['uid']);//累计充值
        $user['orderStatusSum'] = StoreOrder::getOrderStatusSum($user['uid']);//累计消费
        $user['extractTotalPrice'] = UserExtract::userExtractTotalPrice($user['uid']);//累计提现
        $user['extractPrice'] = $user['brokerage_price'];//可提现
        $user['statu'] = (int)sys_config('store_brokerage_statu');
        $broken_time = intval(sys_config('extract_time'));
        $search_time = time() - 86400 * $broken_time;

        $user['recharge_switch'] = 1;//充值开关
        $user['adminid'] = false;
        if ($user['phone'] && $user['user_type'] != 'h5') {
            $user['switchUserInfo'][] = $user2;
            if ($h5UserInfo = User::where('account', $user['phone'])->where('user_type', 'h5')->find()) {
                $user['switchUserInfo'][] = $h5UserInfo;
            }
        } else if ($user['phone'] && $user['user_type'] == 'h5') {
            if ($wechatUserInfo = User::where('phone', $user['phone'])->where('user_type', '<>', 'h5')->find()) {
                $user['switchUserInfo'][] = $wechatUserInfo;
            }
            $user['switchUserInfo'][] = $user2;
        } else if (!$user['phone']) {
            $user['switchUserInfo'][] = $user2;
        }

        $service_url=sys_config('service_url');
        $user['service_url']=$service_url;

        return app('json')->successful($user);
    }

    /**
     * 地址 获取单个
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function address(Request $request, $id)
    {
        $addressInfo = [];
        if ($id && is_numeric($id) && UserAddress::be(['is_del' => 0, 'id' => $id, 'uid' => $request->uid()])) {
            $addressInfo = UserAddress::find($id)->toArray();
        }
        return app('json')->successful($addressInfo);
    }

    /**
     * 地址列表
     * @param Request $request
     * @param $page
     * @param $limit
     * @return mixed
     */
    public function address_list(Request $request)
    {
        list($page, $limit) = UtilService::getMore([['page', 0], ['limit', 20]], $request, true);
        $list = UserAddress::getUserValidAddressList($request->uid(), $page, $limit, 'id,real_name,phone,province,city,district,detail,is_default');
        return app('json')->successful($list);
    }

    /**
     * 设置默认地址
     *
     * @param Request $request
     * @return mixed
     */
    public function address_default_set(Request $request)
    {
        list($id) = UtilService::getMore([['id', 0]], $request, true);
        if (!$id || !is_numeric($id)) return app('json')->fail('参数错误!');
        if (!UserAddress::be(['is_del' => 0, 'id' => $id, 'uid' => $request->uid()]))
            return app('json')->fail('地址不存在!');
        $res = UserAddress::setDefaultAddress($id, $request->uid());
        if (!$res)
            return app('json')->fail('地址不存在!');
        else
            return app('json')->successful('设置成功');
    }

    /**
     * 获取默认地址
     * @param Request $request
     * @return mixed
     */
    public function address_default(Request $request)
    {
        $defaultAddress = UserAddress::getUserDefaultAddress($request->uid(), 'id,real_name,phone,province,city,district,detail,is_default');
        if ($defaultAddress) {
            $defaultAddress = $defaultAddress->toArray();
            return app('json')->successful('ok', $defaultAddress);
        }
        return app('json')->successful('empty', []);
    }

    /**
     * 修改 添加地址
     * @param Request $request
     * @return mixed
     */
    public function address_edit(Request $request)
    {
        $addressInfo = UtilService::postMore([
            ['address', []],
            ['is_default', false],
            ['real_name', ''],
            ['post_code', ''],
            ['phone', ''],
            ['detail', ''],
            ['id', 0],
            ['type', 0]
        ], $request);
        if (!isset($addressInfo['address']['province'])) return app('json')->fail('收货地址格式错误!');
        if (!isset($addressInfo['address']['city'])) return app('json')->fail('收货地址格式错误!');
        if (!isset($addressInfo['address']['district'])) return app('json')->fail('收货地址格式错误!');
        if (!isset($addressInfo['address']['city_id']) && $addressInfo['type'] == 0) {
            return app('json')->fail('收货地址格式错误!请重新选择!');
        } else if ($addressInfo['type'] == 1 && !$addressInfo['id']) {
            $city = $addressInfo['address']['city'];
            $cityId = SystemCity::where('name', $city)->where('parent_id', '<>', 0)->value('city_id');
            if ($cityId) {
                $addressInfo['address']['city_id'] = $cityId;
            } else {
                if (!($cityId = SystemCity::where('parent_id', '<>', 0)->where('name', 'like', "%$city%")->value('city_id'))) {
                    return app('json')->fail('收货地址格式错误!修改后请重新导入!');
                }
            }
        }

        $addressInfo['province'] = $addressInfo['address']['province'];
        $addressInfo['city'] = $addressInfo['address']['city'];
        $addressInfo['city_id'] = $addressInfo['address']['city_id'] ?? 0;
        $addressInfo['district'] = $addressInfo['address']['district'];
        $addressInfo['is_default'] = (int)$addressInfo['is_default'] == true ? 1 : 0;
        $addressInfo['uid'] = $request->uid();
        unset($addressInfo['address'], $addressInfo['type']);
        try {
            validate(AddressValidate::class)->check($addressInfo);
        } catch (ValidateException $e) {
            return app('json')->fail($e->getError());
        }
        //编辑
        if ($addressInfo['id'] && UserAddress::be(['id' => $addressInfo['id'], 'uid' => $request->uid(), 'is_del' => 0])) {
            $id = $addressInfo['id'];
            unset($addressInfo['id']);
            if ($addressInfo['city_id'] == 0)
                unset($addressInfo['city_id']);
            if (UserAddress::edit($addressInfo, $id, 'id')) {
                if ($addressInfo['is_default'])
                    UserAddress::setDefaultAddress($id, $request->uid());
                return app('json')->successful('保存成功');
            } else
                return app('json')->fail('编辑收货地址失败!');
        } else {
            unset($addressInfo['id']);
            $addressInfo['add_time'] = time();
            if ($address = UserAddress::create($addressInfo)) {
                if ($addressInfo['is_default']) {
                    UserAddress::setDefaultAddress($address->id, $request->uid());
                }
                return app('json')->successful('添加成功',['id' => $address->id]);
            } else {
                return app('json')->fail('添加收货地址失败!');
            }
        }
    }

    /**
     * 删除地址
     *
     * @param Request $request
     * @return mixed
     */
    public function address_del(Request $request)
    {
        list($id) = UtilService::postMore([['id', 0]], $request, true);
        if (!$id || !is_numeric($id)) return app('json')->fail('参数错误!');
        if (!UserAddress::be(['is_del' => 0, 'id' => $id, 'uid' => $request->uid()]))
            return app('json')->fail('地址不存在!');
        if (UserAddress::edit(['is_del' => '1'], $id, 'id'))
            return app('json')->successful('删除成功');
        else
            return app('json')->fail('删除地址失败!');
    }


    /**
     * 获取收藏产品
     *
     * @param Request $request
     * @return mixed
     */
    public function collect_user(Request $request)
    {
        list($page, $limit) = UtilService::getMore([
            ['page', 0],
            ['limit', 0]
        ], $request, true);
        if (!(int)$limit) return app('json')->successful([]);
        $productRelationList = StoreProductRelation::getUserCollectProduct($request->uid(), (int)$page, (int)$limit);
        return app('json')->successful($productRelationList);
    }

    /**
     * 添加收藏
     * @param Request $request
     * @param $id
     * @param $category
     * @return mixed
     */
    public function collect_add(Request $request)
    {
        list($id, $category) = UtilService::postMore([['id', 0], ['category', 'product']], $request, true);
        if (!$id || !is_numeric($id)) return app('json')->fail('参数错误');
        $res = StoreProductRelation::productRelation($id, $request->uid(), 'collect', $category);
        if (!$res) return app('json')->fail(StoreProductRelation::getErrorInfo());
        else return app('json')->successful();
    }

    /**
     * 取消收藏
     *
     * @param Request $request
     * @return mixed
     */
    public function collect_del(Request $request)
    {
        list($id, $category) = UtilService::postMore([['id', 0], ['category', 'product']], $request, true);
        if (!$id || !is_numeric($id)) return app('json')->fail('参数错误');
        $res = StoreProductRelation::unProductRelation($id, $request->uid(), 'collect', $category);
        if (!$res) return app('json')->fail(StoreProductRelation::getErrorInfo());
        else return app('json')->successful();
    }

    /**
     * 批量收藏
     * @param Request $request
     * @return mixed
     */
    public function collect_all(Request $request)
    {
        $collectInfo = UtilService::postMore([
            ['id', []],
            ['category', 'product'],
        ], $request);
        if (!count($collectInfo['id'])) return app('json')->fail('参数错误');
        $productIdS = $collectInfo['id'];
        $res = StoreProductRelation::productRelationAll($productIdS, $request->uid(), 'collect', $collectInfo['category']);
        if (!$res) return app('json')->fail(StoreProductRelation::getErrorInfo());
        else return app('json')->successful('收藏成功');
    }

    /**
     * 添加点赞
     *
     * @param Request $request
     * @return mixed
     */
//    public function like_add(Request $request)
//    {
//        list($id, $category) = UtilService::postMore([['id',0], ['category','product']], $request, true);
//        if(!$id || !is_numeric($id))  return app('json')->fail('参数错误');
//        $res = StoreProductRelation::productRelation($id,$request->uid(),'like',$category);
//        if(!$res) return  app('json')->fail(StoreProductRelation::getErrorInfo());
//        else return app('json')->successful();
//    }

    /**
     * 取消点赞
     *
     * @param Request $request
     * @return mixed
     */
//    public function like_del(Request $request)
//    {
//        list($id, $category) = UtilService::postMore([['id',0], ['category','product']], $request, true);
//        if(!$id || !is_numeric($id)) return app('json')->fail('参数错误');
//        $res = StoreProductRelation::unProductRelation($id, $request->uid(),'like',$category);
//        if(!$res) return app('json')->fail(StoreProductRelation::getErrorInfo());
//        else return app('json')->successful();
//    }

    /**
     * 签到 配置
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function sign_config()
    {
        $signConfig = sys_data('sign_day_num') ?? [];
        return app('json')->successful($signConfig);
    }

    /**
     * 签到 列表
     * @param Request $request
     * @param $page
     * @param $limit
     * @return mixed
     */
    public function sign_list(Request $request)
    {
        list($page, $limit) = UtilService::getMore([
            ['page', 0],
            ['limit', 0]
        ], $request, true);
        if (!$limit) return app('json')->successful([]);
        $signList = UserSign::getSignList($request->uid(), (int)$page, (int)$limit);
        if ($signList) $signList = $signList->toArray();
        return app('json')->successful($signList);
    }

    /**
     * 签到
     * @param Request $request
     * @return mixed
     */
    public function sign_integral(Request $request)
    {
        $signed = UserSign::getIsSign($request->uid());
        if ($signed) return app('json')->fail('已签到');
        if (false !== ($integral = UserSign::sign($request->uid())))
            return app('json')->successful('签到获得' . floatval($integral) . '积分', ['integral' => $integral]);
        return app('json')->fail(UserSign::getErrorInfo('签到失败'));
    }

    /**
     * 签到用户信息
     * @param Request $request
     * @return mixed
     */
    public function sign_user(Request $request)
    {
        list($sign, $integral, $all) = UtilService::postMore([
            ['sign', 0],
            ['integral', 0],
            ['all', 0],
        ], $request, true);
        $user = User::getUserInfo($request->uid(),'uid,nickname,avatar,now_money,brokerage_price,integral,sign_num,pay_count,adminid,login_type');
        //是否统计签到
        if ($sign || $all) {
            $user['sum_sgin_day'] = UserSign::getSignSumDay($user['uid']);
            $user['is_day_sgin'] = UserSign::getIsSign($user['uid']);
            $user['is_YesterDay_sgin'] = UserSign::getIsSign($user['uid'], 'yesterday');
            if (!$user['is_day_sgin'] && !$user['is_YesterDay_sgin']) {
                $user['sign_num'] = 0;
            }
        }
        //是否统计积分使用情况
        if ($integral || $all) {
            $user['sum_integral'] = (int)UserBill::getRecordCount($user['uid'], 'integral', 'sign,system_add,gain');
            $refund_integral = (int)UserBill::where(['uid' => $user['uid'], 'category' => 'integral', 'status' => 1, 'pm' => 0])->where('type', 'in', 'sign,system_add,gain')->sum('number');
            if ($user['sum_integral'] > $refund_integral)
                $user['sum_integral'] = $user['sum_integral'] - $refund_integral;
            else
                $user['sum_integral'] = 0;
            $user['deduction_integral'] = (int)UserBill::getRecordCount($user['uid'], 'integral', 'deduction', '', true) ?? 0;
            $user['today_integral'] = (int)UserBill::getRecordCount($user['uid'], 'integral', 'sign,system_add,gain', 'today');
        }
        unset($user['pwd']);
        return app('json')->successful($user);
    }

    /**
     * 签到列表（年月）
     *
     * @param Request $request
     * @return mixed
     */
    public function sign_month(Request $request)
    {
        list($page, $limit) = UtilService::getMore([
            ['page', 0],
            ['limit', 0]
        ], $request, true);
        if (!$limit) return app('json')->successful([]);
        $userSignList = UserSign::getSignMonthList($request->uid(), (int)$page, (int)$limit);
        return app('json')->successful($userSignList);
    }

    /**
     * 获取活动状态
     * @return mixed
     */
    public function activity()
    {
        $data['is_bargin'] =  false;
        $data['is_pink'] =  false;
        $data['is_seckill'] =  false;
        return app('json')->successful($data);
    }

    /**
     * 用户修改信息
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        list($avatar, $nickname) = UtilService::postMore([
            ['avatar', ''],
            ['nickname', ''],
        ], $request, true);
        $nickname=trim(filter_emoji($nickname));
        if($nickname=='') app('json')->fail('昵称不能为空');

        if (User::editUser($avatar, $nickname, $request->uid())){
            $data=['nickname'=>$nickname,'avatar'=>$avatar];
            User::upUserInfoByRedis($request->uid(),$data);
            return app('json')->successful('修改成功');
        }
        return app('json')->fail('修改失败');
    }

}