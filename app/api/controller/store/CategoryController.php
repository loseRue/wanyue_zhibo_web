<?php

namespace app\api\controller\store;

use app\models\store\StoreCategory;
use app\Request;
use app\Redis;

class CategoryController
{
    /*public function category(Request $request)
    {
        $cateogry = StoreCategory::with('children')->where('is_show', 1)->order('sort desc,id desc')->where('pid', 0)->select();
        return app('json')->success($cateogry->hidden(['add_time', 'is_show', 'sort', 'children.sort', 'children.add_time', 'children.pid', 'children.is_show'])->toArray());
    }*/

    public function category(Request $request)
    {

        $list=StoreCategory::getList();
        $list=$this->handelCate($list);

        return app('json')->success($list);
    }

    protected function handelCate($data,$pid=0){
        $list=[];
        foreach ($data as $k=>$v){
            if($v['pid']==$pid){
                unset($data[$k]);
                $v['children']=$this->handelCate($data,$v['id']);
                $list[]=$v;
            }
        }
        return $list;
    }
}